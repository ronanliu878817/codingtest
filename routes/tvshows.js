const express = require('express');
const isArray = require('lodash/isArray');
const isPlainObject = require('lodash/isPlainObject');

const router = express.Router();

router.post('/', (req, res, next) => {
  const data = isArray(req.body.payload) ? req.body.payload : [];
  try {
    const response = data
      .filter(
        record => isPlainObject(record)
          && record.drm === true
          && record.episodeCount > 0
      )
      .map((record) => {
        const { image, slug, title } = record;
        const imgSrc = isPlainObject(image) ? image.showImage : '';
        return {
          image: imgSrc,
          slug,
          title,
        };
      });

    res.json({ response });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
