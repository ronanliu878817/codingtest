const { spawn } = require('child_process');
const request = require('request');
const test = require('tape');
const isEqual = require('lodash/isEqual');

const sampleRequest = require('./sampleRequest');
const sampleResponse = require('./sampleResponse');

const env = Object.assign({}, process.env, { PORT: 5000 });
const defaultOptions = {
  url: 'http://127.0.0.1:5000/tvshows',
  method: 'POST',
  json: true,
  body: {},
};

test('Request with sample data', (t) => {
  t.plan(3);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = {
      ...defaultOptions,
      body: sampleRequest,
    };
    request(options, (error, response, body) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 200);
      t.ok(isEqual(body, sampleResponse));
    });
  });
});

test('Request with incorrect url', (t) => {
  t.plan(3);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = {
      ...defaultOptions,
      url: 'http://127.0.0.1:5000/',
    };
    request(options, (error, response, body) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 404);
      t.equal(body.error, 'Not Found');
    });
  });
});

test('Request with incorrect method', (t) => {
  t.plan(3);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = {
      ...defaultOptions,
      method: 'PATCH',
    };
    request(options, (error, response, body) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 404);
      t.equal(body.error, 'Not Found');
    });
  });
});

test('Request with valid JSON', (t) => {
  t.plan(2);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = defaultOptions;
    request(options, (error, response) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 200);
    });
  });
});

test('Request with invalid JSON', (t) => {
  t.plan(3);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = {
      ...defaultOptions,
      body: '{ payload: [] }',
    };
    request(options, (error, response, body) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 400);
      t.equal(body.error, 'Could not decode request: JSON parsing failed');
    });
  });
});

test('Request without "payload" key in request body', (t) => {
  t.plan(3);

  const child = spawn('node', ['app.js'], { env });
  child.stdout.on('data', () => {
    const options = defaultOptions;
    request(options, (error, response, body) => {
      child.kill();
      t.false(error);
      t.equal(response.statusCode, 200);
      t.ok(isEqual(body, { response: [] }));
    });
  });
});
