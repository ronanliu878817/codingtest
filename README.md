# CodingTest

A simple nodejs service for a coding test.

## Requirements

- Setup a nodejs standalone service on any available platform, e.g. AWS, Heroku.
- Deploy to a specific platform with an accessible url.
- Receive a POST request with a JSON payload, process and send back with a JSON response.

## File Structure

- root
  - routes
    - tvshows.js
  - tests
    - sampleRequest.js
    - sampleResponse.js
    - tvshows.test.js
  - package.json
  - .gitignore
  - app.js
  - other heroku files
