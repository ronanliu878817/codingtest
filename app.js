const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const createError = require('http-errors');
const tvShowsRouter = require('./routes/tvshows');

const PORT = process.env.PORT || 5000;

const app = express();

// Apply middlewares
app.use(cors());
app.use(bodyParser.json());
app.use((err, req, res, next) => next(createError(400, 'Could not decode request: JSON parsing failed')));

// Register routes
app.use('/tvshows', tvShowsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => next(createError(404)));

// customized error handler
app.use((err, req, res, next) => res.status(err.status || 500).json({ error: err.message }));

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
